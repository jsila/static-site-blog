(function($) {

const showAllContent = event => {
    event.preventDefault()

    const btn$ = $(event.target)
    const entryContent$ = btn$.closest('.entry-content')

    let paragraphsBefore = true
    entryContent$.children().each((i, p) => {
        if (event.target.parentNode === p) {
            paragraphsBefore = false
        }
        if (paragraphsBefore) return

        $(p).toggle()
    })
}

$.fn.footnotes = function() {
    $(this).each((articleId, article) => {
        articleId = articleId + 1

        const content = $('.entry-content', article),
            footnotes = []

        content.find('footnote').each((i, fn) => {
            fn = $(fn)
            footnotes.push(fn.text())
            fn.replaceWith(`<sup><a href="#footnote_${articleId}_${i+1}" id="footnote_id_${articleId}_${i+1}" class="footnote-id footnote-id-link" title="${fn.text()}">${i+1}</a></sup>`)
        })

        if (footnotes.length === 0) return

        const lis = footnotes.map((fntext, i) => `<li id="footnote_${articleId}_${i+1}">${fntext}<a href="#footnote_id_${articleId}_${i+1}">&crarr;</a></li>`)

        content.append([
            '<hr class="footnotes-sep" />',
            `<ol class="footnotes">${lis}</ol>`
        ])
    })
}

$('article.entry').footnotes()
$('.home .continue-reading').on('click', showAllContent)

// let disqusArticle$

// const showDisqus = event => {
//     event.preventDefault()

//     const btn$ = $(event.target)
//     const article$ = btn$.closest('article.entry')

//     const {identifier, url, title} = articles[btn$.data('article-id')]

//     var disqus_shortname = 'testna-stran-moja';
//     var disqus_identifier = identifier;
//     var disqus_url = `http://localhost:8080/#!${disqus_identifier}`;
//     var disqus_title = title;

//     if (window.DISQUS) {
//         disqusArticle$.find('#disqus_thread').remove()
//         article$.append('<div id="disqus_thread"></div>')

//         DISQUS.reset({
//             reload: true,
//             config: function() {
//                 this.page.identifier = disqus_identifier
//                 this.page.url = disqus_url
//                 this.page.title = disqus_title
//             }
//         })

//     } else {
//         article$.append('<div id="disqus_thread"></div>')

//         var disqus_config = function () {
//                 this.page.identifier = disqus_identifier
//                 this.page.url = disqus_url
//                 this.page.title = disqus_title
//         }
//         const s = document.createElement('script');
//         s.src = `//${disqus_shortname}.disqus.com/embed.js`;
//         s.setAttribute('data-timestamp', +new Date());
//         s.async = true;
//         (document.head || document.body).appendChild(s);
//     }

//     disqusArticle$ = article$    
// }

// $('.comments-link').on('click', showDisqus)

})($)