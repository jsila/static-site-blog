const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const {resolve} = require('path');

const resolveSrc = (...path) => resolve('src', ...path);

const extractSass = new ExtractTextPlugin({
    filename: "main.css",
    // filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});

const webpackConfig = {
    entry: [
        resolveSrc('styles', 'main.scss'),
        resolveSrc('scripts', 'main.js'),
        resolveSrc('styles', 'prism.css'),
        resolveSrc('scripts', 'prism.js'),
    ],
    output: {
        path: resolve('site', 'build'),
        filename: 'main.js'
        // filename: '[name].[hash].js'
    },
    devtool: "source-map",
    module: {
        rules: [{
            test: /\.s?css$/,
            use: extractSass.extract({
                use: [
                    { loader: "css-loader", options: { sourceMap: true } },
                    { loader: "sass-loader", options: { sourceMap: true } }
                ],
                fallback: "style-loader"
            })
        }, {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: { loader: 'babel-loader' }
        }]
    },
    plugins: [
        extractSass
    ],
    devServer: {
        contentBase: resolve('site', 'build')
    }
};

if (process.env.NODE_ENV === "development") {
    webpackConfig.entry.push('webpack-dev-server/client?http://localhost:8080');
    // webpackConfig.output = {
    //     path: resolve('public'),
    //     filename: 'main.js'
    // };
    //
    // webpackConfig.plugins.push(new HtmlWebpackPlugin({ filename: 'index.html', template: resolveSrc('index.html') }));
    // webpackConfig.plugins.push(new HtmlWebpackPlugin({ filename: 'single.html', template: resolveSrc('single.html') }));
    // webpackConfig.plugins.push(new HtmlWebpackPlugin({ filename: 'tags.html', template: resolveSrc('tags.html') }));
}

module.exports = webpackConfig;