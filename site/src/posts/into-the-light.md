---
title: "Review: Into the light by Aleatha Romig"
description: My review of the book by Shari Lepena, The Couple Next Door
keywords: light aleatha romig
date: 2017-06-11
thumbnail:
 src: aleatha-romig-into-the-light.jpg
 alt: Book cover of Into the Light
tags: 
 - aleatharomig
 - books
layout: single.hbs
---
Outstanding writing! Amazing, engaging and unique story! Full of suspense and intrigue. My mind was blown away, my hands were sweaty, my stomach was in knots. This incredible book is like NO OTHER I’ve ever read. Aleatha Romig is the artist behind this masterpiece. I warn you that there is NO way you can put this book down once you start! You’ve been in “the dark” and now you see “The Light”. I was completely immersed in the story and I didn’t want to escape! I feel like I do not have the proper words to describe how refreshing this story was, how different it was! The combination between suspense, intrigue, romance, mystery, and deception makes this book one of the best reads of the year for me thus far!

<p class="continue-reading"><a href="#">Continue reading</a></p>

I still can not believe I spent my life without knowing this author! How is it even possible that I’ve never read anything by her before??? Of course I had the best references from Aleatha books, but I never had the chance to read one. Thankfully our blog got the opportunity to review “Into the Light”, so I FINALLY had my chance! I love mystery and thrillers and as soon as I read the fascinating blurb, I was very sold! I asked (almost begged) for it to review and I LOVED every minute of it!

What can I say, since the first page my mind was plotting different theories and as the story continued, I got more and more questions! I was aware to expect the unexpected with Aleatha’s stories and let me tell you that it was all true!”Into the Light” is told from multiple POV’s, so we can have different scenarios and sides of the story.

“Into The Light” is the first book of this duet. If you’re looking for an elegant writing with a lot of vivid imagery and a highly mysterious and intriguing storyline, THIS is the book for you! I’m really counting the days until we have the second book: “Away from the Dark (The Light #2)”.