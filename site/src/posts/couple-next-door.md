---
title: "Review: The Couple Next Door"
description: My review of the book by Shari Lepena, The Couple Next Door
keywords: The Couple Next Door
date: 2017-06-16
layout: single.hbs
tags:
 - books
 - sharilapena
---

I will be writing my impressions about this book gradually, after reading every couple of chapters. It's easier to gather impressions for me that way because if I give them at the end, some may be forgotten and to me it would look incomplete and awful.

<p class="continue-reading"><a href="#">Continue reading</a></p>

Reading first couple of pages felt a bit off and than I realized it's written in present tense. I don't remember if I've ever read a book that way. Another oddities are consecutive sentences starting with the same character name (like the author doesn't know how to rephrase sentence so it doesn't start the same way). It seems to be a detective type of novel because we have a missing baby and a detective who's very thorough in asking the parents all kinds of questions, even the types that hint parents involvement into baby's disappearance. The story quickly sucked me in.