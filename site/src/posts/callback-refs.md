---
title: Callback refs as componentDidMount in stateless functions in React
description: Here is one to use callback ref in stateless function components in React
keywords: callback ref
date: 2017-05-12
tags: 
 - webdevelopment
 - react
layout: single.hbs
---

You might think that when developing user interfaces with React and if you want to e.g. apply jQuery method to an element, you must abandon your beloved stateless function in favor of class-based component, directly or indirectly via higher-order components, because stateless function doesn't have <code>componentDidMount</code> life-cycle hook. You are wrong, you can use [callback refs](https://facebook.github.io/react/docs/refs-and-the-dom.html) for that where you are given DOM instance of that element.

<p class="continue-reading"><a href="#">Continue reading</a></p>

So let's say you are using [Semantic-UI](https://semantic-ui.com) toolkit which provides jQuery methods to bootstrap functionalities like modals, accordions, tabs, etc. that needs to be called after node's rendered and it's DOM instance is available. So if you want to apply accordion functionality, you can do it like this.

<pre class="language-jsx line-numbers"><code>&lt;div className="ui styled fluid accordion" ref={el => $(el).accordion()}>&lt;/div&gt;
  &lt;div className="title"&gt;&lt;/div&gt;
    &lt;i className="dropdown icon"&gt;&lt;/i&gt;&lt;/i&gt; Title
  &lt;/div&gt;
  &lt;div className="content"&gt;&lt;/div&gt;
    Content
  &lt;/div&gt;
&lt;/div&gt;</code></pre>

I use this approach quite often. But I stick to some ground rules while using it: (1) in callback ref I only manipulate with referenced element, because if I manipulate with other elements, there might be bugs due to other elements not being rendered yet and (2) I keep lines of code in callback ref as minimum as possible, preferably one-line.
