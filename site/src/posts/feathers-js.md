---
title: "Feathers.js: Develop RESTful and realtime apps easier"
description: I've found this great library for Node.js I can't wait to write about.
keywords: Feathers.js
date: 2017-03-02
tags:
 - webdevelopment
 - api
 - libraries
layout: single.hbs
---
I discovered [Feathers.js](https://feathersjs.com) via [Tutorialzine](https://tutorialzine.com) article that popped in my Facebook timeline, which offered [15 interesting JavaScript and CSS libraries for June 2017](https://tutorialzine.com/2017/06/15-interesting-javascript-and-css-libraries-for-june-2017). Since then I've been toying with it for almost a week now and now I'm really starting to like it.

<p class="continue-reading"><a href="#">Continue reading</a></p>

It has a lot of [documentation](https://docs.feathersjs.com) to start with but I wouldn't recommend it to someone new to web development since it's not straightforward, there are a lot of concept to be familiarize with (web development-wise) and it takes some time to learn how it's supposed to work. In other words, it has some learning curve. But after I connected the dots I started appreciate it because it enables me to quickly bootstrap common tasks while building a web applications (like authentication).

Feathers has a service and hook-based approach when developing web applications. A service takes care of a REST resource and out-of-the-box you can perform all standard operations on it (find all resources, get single resource, or update and delete it). Hooks are where business logic around resource is designed to reside. Hooks are functions that get executed before and after this "out-of-the-box operations" are executed. You can define hooks resource-wise or app-wise (i.e. when user is created or when a resource is created). Hooks should be small and do one thing only. When first learning about them, they reminded me of functions in functional programming which are small and single-purpose too.

So for example if you are writing hook for validating form request, you supply it a validation rules and if rules are not met, you throw an error otherwise request is passed to the next hook in chain. You don't use the same hook to execute logic in case of validation rules being met because you can use this "form request validation" hook for multiple resources, right? Instead you define another hook as a hook next to this validation hook. When you throw an error in some hook, the rest of them that follows don't get executed (you can take this as a chain of responsibility design pattern <footnote>It's similar to decorator pattern with that difference that any link in a chain can interrupt the execution.</footnote>).

Aside from create hook there are also update, patch, delete, put, all (accounts for all hooks), find, get.

Another cool thing about Feathers is that it provides browser-side client which ease interacting with feathers-based API. You don't event need to deal with underlying library that makes AJAX requests - just select which one you want client to use (fetch, axios to name a few).