const tinytime = require('tinytime');

const formatDate = function(opts = {}) {
    if (!opts.template) throw Error("missing template for tinytime rendering");

    const {template, tinyTimeOpts, rewrite} = Object.assign({
        tinyTimeOpts: {},
        rewrite: false
    }, opts);
    const key = rewrite ? 'date' : 'date_string';

    return function(files, metalsmith, done) {
        setImmediate(done);
        const t = tinytime(template, tinyTimeOpts);
        Object.keys(files).forEach(file => {
            const {date} = files[file];
            if (!date) return;
            files[file][key] = t.render(date);
        })
    }
};

module.exports = formatDate;