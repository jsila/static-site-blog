const stopwords = [
    'about', 'after', 'all', 'also', 'am', 'an', 'and', 'another', 'any', 'are', 'as', 'at', 'be',
    'because', 'been', 'before', 'being', 'between', 'both', 'but', 'by', 'came', 'can',
    'come', 'could', 'did', 'do', 'each', 'for', 'from', 'get', 'got', 'has', 'had',
    'he', 'have', 'her', 'here', 'him', 'himself', 'his', 'how', 'if', 'in', 'into',
    'is', 'it', 'like', 'make', 'many', 'me', 'might', 'more', 'most', 'much', 'must',
    'my', 'never', 'now', 'of', 'on', 'only', 'or', 'other', 'our', 'out', 'over',
    'said', 'same', 'see', 'should', 'since', 'some', 'still', 'such', 'take', 'than',
    'that', 'the', 'their', 'them', 'then', 'there', 'these', 'they', 'this', 'those',
    'through', 'to', 'too', 'under', 'up', 'very', 'was', 'way', 'we', 'well', 'were',
    'what', 'where', 'which', 'while', 'who', 'with', 'would', 'you', 'your', 'a', 'i'];

const removeStopwords = function(opts = {}) {
    return function(files, metalsmith, done) {
        setImmediate(done);
        Object.keys(files).forEach(file => {
            const data = files[file];

            if (!data.collection || !data.collection.includes('posts')) return;

            const [path, filename] = file.split('/');

            data.path = path
                .split('-')
                .filter(w => !stopwords.includes(w))
                .join('-');

            const out = data.path + '/' + filename;

            delete files[file];
            files[out] = data;
        });
        done()
    }
};

module.exports = removeStopwords;