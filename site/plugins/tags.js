const _ = require('lodash');

const tags = () => {

    const sortBy = attr => (a,b) => {
        if (a[attr] < b[attr]) return -1;
        if (a[attr] > b[attr]) return 1;
        return 0;
    };

    return (files, metalsmith, done) => {
        setImmediate(done);

        const metadata = metalsmith.metadata();
        const posts_by_tags = _.reduce(files, (result, data) => {
            if (!data.collection || !data.collection.includes('posts')) return result;

            let postTags = data.tags || [];

            postTags.forEach(tag => {
                if (!result[tag]) {
                    result[tag] = []
                }
                result[tag].push(data)
            });

            return result;
        }, {});

        metadata.tags = _.map(posts_by_tags, (posts, tag) => ({
            tag,
            count: posts.length,
            file: `tags/${tag}/index.html`
        })).sort(sortBy('date'));

        _.forEach(posts_by_tags, (posts, tag) => {
            files[`tags/${tag}/index.html`] = {
                posts,
                tag,
                contents: "",
                layout: "tag.hbs",
            }
        });
    };
};

module.exports = tags;