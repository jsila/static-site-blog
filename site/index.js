const metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');
const collections = require('metalsmith-collections');
const permalinks = require('metalsmith-permalinks');
const markdown = require('metalsmith-markdown');

const tags = require('./plugins/tags');
const formatDate = require('./plugins/tinytime');
const seoTitles = require('./plugins/stopwords');

const Handlebars = require('handlebars');

Handlebars.registerHelper('page_type', layout => layout.split('.')[0].trim());
Handlebars.registerHelper('if_other_tag', function(opts) {
    const {tag} = opts.data.root;
    if (String(this) === tag) return;
    return opts.fn(this)
})

Handlebars.registerHelper("partial", function (name, options) {
    Handlebars.registerPartial(name, options.fn);
});

Handlebars.loadPartial = function (name) {
    let partial = Handlebars.partials[name];
    if (typeof partial === "string") {
        partial = Handlebars.compile(partial);
        Handlebars.partials[name] = partial;
    }
    return partial;
};

Handlebars.registerHelper("block", function (name, options) {
    const partial = Handlebars.loadPartial(name) || options.fn;
    return partial(this, {
        data: options.hash
    });
});

metalsmith(__dirname)
    .metadata({
        site: {
            title: "All the things I enjoy",
            description: "My profession, entertainment and my thoughts",
            url: 'http://localhost:8080'
        },
        author: 'Jernej Sila',
        keywords: 'Jernej Sila, blog, profession, entertainment, thoughts',
        footer: "Website created by Jernej Sila. All right reserved for text content, because pictures are taken from the Internet."
    })
    .source('./src')
    .destination('./build')
    .clean(true)
    .use(collections({
        posts: {
            pattern: 'posts/*.md',
            sortBy: 'date',
            reverse: true
        }
    }))
    .use(tags())
    .use(markdown())
    .use(permalinks({
        relative: false,
        pattern: ':title'
    }))
    .use(seoTitles())
    .use(formatDate({
        template: '{MMMM} {Do}, {YYYY}',
        tinyTimeOpts: {
            padHours: true
        },
        key: 'date_string'
    }))
    .use(layouts({
        engine: 'handlebars',
        partials: 'partials',
        preventIndent: true
    }))
    .build(function(err) {
        if (err) throw err;
    });